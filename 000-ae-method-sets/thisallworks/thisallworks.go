package thisallworks

import "fmt"

type animal struct {
	Name string
}

func (a animal) np() {
	fmt.Println("non-pointer method", a.Name)
}

func (pa *animal) p() {
	fmt.Println("pointer method", pa.Name)
}

func RunMe() {
	x := &animal{"Bird"}
	x.np()
	x.p()

	y := animal{"Dolphin"}
	y.np()
	y.p()
}
