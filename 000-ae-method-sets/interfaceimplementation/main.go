package interfaceimplementation

import "fmt"

type person struct {
	Name string
}

type secretagent struct {
	person
	Number string
}

func (p person) speak() {
	fmt.Println("I'm a civilian and my name is", p.Name)
}

func (sa secretagent) speak() {
	fmt.Println("I work for the Majesty's secret service and my name is", sa.Name, "and my number is", sa.Number)
}

type human interface {
	speak()
}

func saysomething(h human) {
	h.speak()
}

func ThisWorks() {

	p1 := person{"Miss Moneypenny"}
	p1.speak()
	saysomething(p1)

	sa1 := secretagent{person{"James "}, "007"}
	sa1.speak()
	saysomething(sa1)
}

func (p *person) joke() {
	fmt.Println("Here is a joke from a civilian", p.Name)
}

func (sa *secretagent) joke() {
	fmt.Println("Here is a joke from a secret agent", sa.Name, sa.Number)
}

type comedian interface {
	joke()
}

func tellajoke(c comedian) {
	c.joke()
}

func ThisDoesNotWork() {

	p2 := person{"Miss Moneypenny"}
	fmt.Println("calling the method works")
	p2.joke()
	//tellajoke(p2)

	sa2 := secretagent{person{"James "}, "007"}
	fmt.Println("calling the method works")
	sa2.joke()
	//tellajoke(sa2)
}

func ThisAlsoAllWorks() {
	p3 := person{"Miss Moneypenny"}
	p3.speak()
	saysomething(p3)

	sa3 := secretagent{person{"James "}, "007"}
	sa3.speak()
	saysomething(sa3)

	p4 := &person{"Miss Moneypenny"}
	p4.speak()
	saysomething(p4)

	sa4 := &secretagent{person{"James "}, "007"}
	sa4.speak()
	saysomething(sa4)

	p5 := person{"Miss Moneypenny"}
	p5.joke()
	tellajoke(&p5)

	sa5 := &secretagent{person{"James "}, "007"}
	sa5.joke()
	tellajoke(sa5)
}
