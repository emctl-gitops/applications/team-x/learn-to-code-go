package interfaceimp

import "fmt"

type person struct {
	Name string
}

func (p person) speak() {
	fmt.Println("I'm a civilian and my name is", p.Name)
}

func (p *person) joke() {
	fmt.Println("Here is a joke", p.Name)
}

type human interface {
	speak()
}

type comedian interface {
	joke()
}

func saysomething(h human) {
	h.speak()
}

func tellajoke(c comedian) {
	c.joke()
}

func Runnit() {

	p := person{"James"}
	p.speak()
	p.joke()
	saysomething(p)

	pp := &p

	pp.speak()
	saysomething(pp)

	tellajoke(pp)
}
