package main

import (
	"fmt"
)

type person struct {
	first string
	age   int
}

func main() {

	xi := []int{1, 2, 3}
	fmt.Println(xi)
	changeSlice(xi, 2)
	fmt.Println(xi)

	m := map[string]int{
		"dog":  1,
		"cat":  2,
		"bird": 3,
	}
	fmt.Println(m)
	changeMap(m, "cat")
	fmt.Println(m)

	n := 42
	fmt.Println(n)
	changeValue(n)
	fmt.Println(n)
	changeValue2(&n)
	fmt.Println("again ", n)

	p1 := person{"James", 20}

	fmt.Println(p1)
	changePerson(p1)
	fmt.Println(p1)
	changePerson2(&p1)
	fmt.Println(p1)
	p1.changePerson3()
	fmt.Println(p1)
	p1.changePerson4()

	p2, err := newPerson("Jim", 22)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("%#v\n", p2)
	}
}

func changeSlice(s []int, i int) {
	s[i] *= 2
}

func changeMap(m map[string]int, s string) {
	m[s] *= 2
}
func changeValue(e int) {
	e *= 2
}

func changeValue2(e *int) {
	*e *= 2
}

func changePerson(p person) {
	p.age = 50
}

func changePerson2(p *person) {
	p.age = 50
}

func (p *person) changePerson3() {
	p.age = 50
}

func (p person) changePerson4() {
	p.age = 52
	fmt.Printf("%#v\n", p)
}

func newPerson(s string, n int) (*person, error) {
	if s == "" {
		return nil, fmt.Errorf("person cannot be empty")
	}
	if n <= 0 {
		return nil, fmt.Errorf("age cannot be negative or zero")
	}
	p := person{s, n}
	return &p, nil
}
