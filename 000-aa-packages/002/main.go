package main

import (
	"fmt"
	"mymodule/000-aa-packages/002/testmodule"
)

func main() {
	fmt.Println("From 0002")
	testmodule.PrintHello()
}
