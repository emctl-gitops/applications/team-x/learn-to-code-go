package main

import "fmt"

type person struct {
	first string
}

func (p person) String() string {
	return fmt.Sprint("My name is ", p.first)
}

func main() {
	p1 := person{"James"}
	fmt.Println(p1)
}
