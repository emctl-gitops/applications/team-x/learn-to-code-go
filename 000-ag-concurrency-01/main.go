package main

import ce "mymodule/000-ag-concurrency-01/concurrencyexamples"

func main() {
	ce.One()
	ce.Two()
	ce.Three()
	ce.Four()
	ce.Five()
}
