package concurrencyexamples

import (
	"fmt"
	"sync/atomic"
)

func One() {
	ch := make(chan int)

	go func() {
		ch <- 42
	}()

	x := <-ch
	fmt.Println(x)
}

func Two() {
	ch := make(chan int)

	for i := 0; i < 1000; i++ {
		go func(n int) {
			ch <- n
		}(i)
	}

	var counter int64
	for i := 0; i < 1000; i++ {
		x := <-ch
		fmt.Println(x)
		atomic.AddInt64(&counter, 1)
	}
	fmt.Println("Counters", atomic.LoadInt64(&counter))
}

func Three() {
	ch := make(chan int)

	go func() {
		for i := 0; i < 10; i++ {
			ch <- i
		}
		close(ch)
	}()

	for i := range ch {
		fmt.Println(i)
	}
}

func Four() {
	ch := make(chan int, 1) // buffered channel
	ch <- 42
	x := <-ch
	fmt.Println(x)

	ch2 := make(chan int, 2) // buffered channel
	ch2 <- 43
	ch2 <- 44
	fmt.Println(<-ch2)
	fmt.Println(<-ch2)
}

func Five() {
	ch1, ch2 := make(chan int), make(chan int)

	go func() {
		ch2 <- 42
	}()

	select {
	case val := <-ch1:
		fmt.Println("got from ch1:", val)
	case val := <-ch2:
		fmt.Println("got from ch2:", val)
	}
}
