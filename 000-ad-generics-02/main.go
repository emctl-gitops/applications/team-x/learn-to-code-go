package main

import "fmt"

func main() {
	fmt.Println("Hello, World!")
	fmt.Println(sumInt(42, 43))
	fmt.Println(sumFloat64(42.42, 43.43))
	fmt.Println(sum[int](42, 43))
	fmt.Println(sum[float64](42.01, 43.02))

}

func sumInt(c, y int) int {
	return c + y
}

func sumFloat64(c, y float64) float64 {
	return c + y
}

func sum[T int | float64](c, y T) T {
	return c + y
}
