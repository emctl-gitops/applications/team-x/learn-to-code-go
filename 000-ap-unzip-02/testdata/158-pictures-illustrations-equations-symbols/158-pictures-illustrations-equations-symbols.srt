﻿1
00:00:00,120 --> 00:00:01,800
- Now we're gonna see
how to insert pictures,

2
00:00:01,800 --> 00:00:04,080
illustrations, equations, and symbols.

3
00:00:04,080 --> 00:00:06,210
And this is this lecture right here

4
00:00:06,210 --> 00:00:08,340
that we're on in our course outline.

5
00:00:08,340 --> 00:00:10,230
And when we come to the Excel workbook

6
00:00:10,230 --> 00:00:11,250
associated with this lecture,

7
00:00:11,250 --> 00:00:13,980
which you can download right
next to this lecture, (laughs)

8
00:00:13,980 --> 00:00:15,720
just in case you don't know that already,

9
00:00:15,720 --> 00:00:17,970
here's workbook 158
pictures, illustrations

10
00:00:17,970 --> 00:00:22,350
equations, and symbols, and we
can insert all of this stuff,

11
00:00:22,350 --> 00:00:26,400
all of this stuff into our
workbooks and our worksheets.

12
00:00:26,400 --> 00:00:28,650
And so we do that under the insert menu.

13
00:00:28,650 --> 00:00:31,140
And you know, we can
insert a picture like this.

14
00:00:31,140 --> 00:00:32,820
We can insert a shape like this.

15
00:00:32,820 --> 00:00:35,760
We can insert an icon
like this alien head.

16
00:00:35,760 --> 00:00:39,330
We can insert a 3D model like
this astronaut right here.

17
00:00:39,330 --> 00:00:43,410
And check this guy out. I can
move him all around. (laughs)

18
00:00:43,410 --> 00:00:45,510
That is actually really cool.

19
00:00:45,510 --> 00:00:48,360
And then we could insert some smart art.

20
00:00:48,360 --> 00:00:51,120
And smart art is like
this deal right here.

21
00:00:51,120 --> 00:00:53,520
Create, sell, vacation.

22
00:00:53,520 --> 00:00:55,110
And we could also do a screenshot.

23
00:00:55,110 --> 00:00:58,243
So I took a screenshot
of my web browser bar

24
00:00:58,243 --> 00:01:00,690
just right up here, I
took a screenshot of that

25
00:01:00,690 --> 00:01:02,550
just by clicking that little button.

26
00:01:02,550 --> 00:01:04,290
We could also insert an equation.

27
00:01:04,290 --> 00:01:07,410
So we have equations over here
and I inserted this equation.

28
00:01:07,410 --> 00:01:10,470
And then we have symbols like
copyright, trademark, R, TM,

29
00:01:10,470 --> 00:01:12,270
which happens to be my initials.

30
00:01:12,270 --> 00:01:14,100
And so let's see some of this in action.

31
00:01:14,100 --> 00:01:15,390
So I'm just gonna go to pictures

32
00:01:15,390 --> 00:01:17,670
and you could grab a
picture off this device.

33
00:01:17,670 --> 00:01:18,810
Or you could do a stock image

34
00:01:18,810 --> 00:01:20,520
or you could search for a picture online.

35
00:01:20,520 --> 00:01:21,990
I'm gonna do a stock image.

36
00:01:21,990 --> 00:01:25,170
And I chose that romantic
couple, but I don't,

37
00:01:25,170 --> 00:01:26,610
oh, there they are right there.

38
00:01:26,610 --> 00:01:28,050
I don't know if I want to do them again.

39
00:01:28,050 --> 00:01:29,460
I think I'll do this one.

40
00:01:29,460 --> 00:01:32,580
'Cause what's better than love? (laughs)

41
00:01:32,580 --> 00:01:35,220
What's better than love?
Love is everything.

42
00:01:35,220 --> 00:01:36,690
It's what everybody's searching for,

43
00:01:36,690 --> 00:01:40,050
just to love and be loved
and that's my perspective.

44
00:01:40,050 --> 00:01:41,310
And when you have a picture, by the way,

45
00:01:41,310 --> 00:01:44,460
just inserted this picture,
you get this picture ribbon.

46
00:01:44,460 --> 00:01:45,600
Picture format ribbon.

47
00:01:45,600 --> 00:01:47,460
And you could come up
here and you could choose,

48
00:01:47,460 --> 00:01:48,540
oh, that's a good look,

49
00:01:48,540 --> 00:01:50,190
you could choose different
looks for your pictures.

50
00:01:50,190 --> 00:01:52,350
So there's the look I'm
going for on that one.

51
00:01:52,350 --> 00:01:55,110
I'm gonna go back to my insert
menu and choose a shape.

52
00:01:55,110 --> 00:01:56,250
And because it's love

53
00:01:56,250 --> 00:01:58,140
I couldn't help but to go with the heart.

54
00:01:58,140 --> 00:01:59,790
And so somewhere in here I saw a heart.

55
00:01:59,790 --> 00:02:00,870
There it is right there.

56
00:02:00,870 --> 00:02:04,500
I'm gonna grab the heart
again. And there's my heart.

57
00:02:04,500 --> 00:02:07,560
And then when it's out
there, I have shape format,

58
00:02:07,560 --> 00:02:09,120
this ribbon right up here.

59
00:02:09,120 --> 00:02:10,320
And I could go and go to, like,

60
00:02:10,320 --> 00:02:13,170
shape fill and I can
make that a big red heart

61
00:02:13,170 --> 00:02:15,930
and make it a little
bit bigger, there we go.

62
00:02:15,930 --> 00:02:18,780
Hold down shift to
constrain the proportions.

63
00:02:18,780 --> 00:02:21,180
Otherwise you get like
funky stuff like that.

64
00:02:21,180 --> 00:02:23,970
Control Z to undo that. I'm
gonna go back to insert.

65
00:02:23,970 --> 00:02:27,840
We did a picture, we did a
shape. I'm gonna do an icon here.

66
00:02:27,840 --> 00:02:29,340
Different icons here.

67
00:02:29,340 --> 00:02:33,870
And I don't know, I
don't know, I don't know.

68
00:02:33,870 --> 00:02:35,550
Maybe just balloons.

69
00:02:35,550 --> 00:02:38,010
All right, so I'm gonna
insert some balloons.

70
00:02:38,010 --> 00:02:40,110
So there's my balloons icon.

71
00:02:40,110 --> 00:02:43,230
And again, I get a new ribbon
up here, graphics format.

72
00:02:43,230 --> 00:02:44,550
So just all these choices,

73
00:02:44,550 --> 00:02:46,320
that spirit of adventure and exploration,

74
00:02:46,320 --> 00:02:48,060
you could just play with them to get them

75
00:02:48,060 --> 00:02:50,310
to look the way you want 'em to look.

76
00:02:50,310 --> 00:02:52,710
And I could go with red again on that.

77
00:02:52,710 --> 00:02:53,880
We can go back to insert.

78
00:02:53,880 --> 00:02:57,510
And we did shapes, we did
icons, gonna do a 3D model.

79
00:02:57,510 --> 00:02:59,610
And we have different choices here.

80
00:02:59,610 --> 00:03:01,500
I went with space last time.

81
00:03:01,500 --> 00:03:02,730
I don't know if love's up here.

82
00:03:02,730 --> 00:03:05,643
We could give it a go and
see what comes up with love.

83
00:03:06,510 --> 00:03:09,060
And I like the astronaut. I can't help it.

84
00:03:09,060 --> 00:03:10,230
That astronaut's pretty awesome.

85
00:03:10,230 --> 00:03:12,420
I'm just gonna hit back and go to space,

86
00:03:12,420 --> 00:03:15,030
and well, we already did the astronaut.

87
00:03:15,030 --> 00:03:16,590
Let's have a little variety, shall we?

88
00:03:16,590 --> 00:03:19,290
I'm gonna go with animals and a dog.

89
00:03:19,290 --> 00:03:20,640
Dogs are kind of like love,

90
00:03:20,640 --> 00:03:23,220
much better than a dinosaur
or a shark. (laughs)

91
00:03:23,220 --> 00:03:26,250
And we'll go with this
guy here. There we go.

92
00:03:26,250 --> 00:03:28,578
So there's my dog, or this dog,

93
00:03:28,578 --> 00:03:32,010
and that is just pretty cool.

94
00:03:32,010 --> 00:03:33,330
And definitely hold down shift

95
00:03:33,330 --> 00:03:35,730
so we don't skew him, make him funky.

96
00:03:35,730 --> 00:03:37,080
All right, I'm gonna go back to insert.

97
00:03:37,080 --> 00:03:40,740
And we did picture shapes,
icons, 3D models, smart art.

98
00:03:40,740 --> 00:03:42,390
And so smart art, you know,

99
00:03:42,390 --> 00:03:44,490
has just different choices in here.

100
00:03:44,490 --> 00:03:47,430
These are great. This is,
like, so little known.

101
00:03:47,430 --> 00:03:51,090
And I love the process, smart art here.

102
00:03:51,090 --> 00:03:52,830
And so like, you know, different,

103
00:03:52,830 --> 00:03:54,510
and actually that one
that we already had up

104
00:03:54,510 --> 00:03:56,790
was right there, that's,
like, one of my favorites

105
00:03:56,790 --> 00:03:59,040
and I'm gonna go with that one there.

106
00:03:59,040 --> 00:04:01,470
And so it's just asking me for some texts.

107
00:04:01,470 --> 00:04:05,340
I'm gonna move it over
here and I'll do up here.

108
00:04:05,340 --> 00:04:09,657
First, record, and then second, students,

109
00:04:10,877 --> 00:04:15,330
and then third vacation, (laughs) right?

110
00:04:15,330 --> 00:04:18,780
There we go. That's the path forward.

111
00:04:18,780 --> 00:04:20,310
That's the path forward.

112
00:04:20,310 --> 00:04:22,170
And then finally we have screenshot.

113
00:04:22,170 --> 00:04:24,660
So I could look in here, I don't know,

114
00:04:24,660 --> 00:04:26,700
maybe these are recent screenshots I took.

115
00:04:26,700 --> 00:04:29,490
Or these are suggestions. I'm
gonna go to screen clipping.

116
00:04:29,490 --> 00:04:32,340
And with screen clipping
it starts to fade out.

117
00:04:32,340 --> 00:04:34,380
And I don't know if I could
switch different stuff.

118
00:04:34,380 --> 00:04:36,453
Like, you know, can I bring that up?

119
00:04:37,730 --> 00:04:40,890
It goes to, I'm not sure how
it chooses where it goes.

120
00:04:40,890 --> 00:04:43,110
I guess it's whatever's behind Excel.

121
00:04:43,110 --> 00:04:46,410
But this time I will grab this.

122
00:04:46,410 --> 00:04:49,080
And yeah, I guess I'll just grab that

123
00:04:49,080 --> 00:04:51,750
and we'll just kind of
truncate it partway through

124
00:04:51,750 --> 00:04:53,156
so we can show it.

125
00:04:53,156 --> 00:04:55,950
So there's my, you know, there's
my little screen clipping

126
00:04:55,950 --> 00:04:59,850
that I just did and kind of made it big.

127
00:04:59,850 --> 00:05:03,240
Now we also have under insert,
we did all these guys here.

128
00:05:03,240 --> 00:05:04,440
We also have equations.

129
00:05:04,440 --> 00:05:06,630
And so there's some
really complicated looking

130
00:05:06,630 --> 00:05:08,700
equations here and I
took a whole lot of math

131
00:05:08,700 --> 00:05:13,200
when I was an an undergraduate
and I studied economics.

132
00:05:13,200 --> 00:05:15,810
But, you know, that one looks particularly

133
00:05:15,810 --> 00:05:17,280
like we know what we're doing.

134
00:05:17,280 --> 00:05:19,470
And you could select all
that and you could go,

135
00:05:19,470 --> 00:05:21,810
you could right click it
and you could choose font

136
00:05:21,810 --> 00:05:23,820
and then you could just make it bigger.

137
00:05:23,820 --> 00:05:25,653
And let's see that in action.

138
00:05:27,210 --> 00:05:28,410
And I guess I have to hit, okay.

139
00:05:28,410 --> 00:05:30,330
I'll do, like, 25.

140
00:05:30,330 --> 00:05:32,820
Enter, there we go. Now it's bigger.

141
00:05:32,820 --> 00:05:35,670
And you can right click it
and format it and all that.

142
00:05:35,670 --> 00:05:38,040
And then finally under
insert, we have symbols.

143
00:05:38,040 --> 00:05:39,510
And those symbols are just, like,

144
00:05:39,510 --> 00:05:44,130
random sort of ASCII, UTF-8 characters.

145
00:05:44,130 --> 00:05:45,960
And there's different symbols down here.

146
00:05:45,960 --> 00:05:50,460
So I'm just gonna go for the
London pound, I guess, I am.

147
00:05:50,460 --> 00:05:53,070
And I got two of them in
there. I just need one.

148
00:05:53,070 --> 00:05:55,350
And then I could select
that and make it bigger too.

149
00:05:55,350 --> 00:05:57,030
It's pretty small right there

150
00:05:57,030 --> 00:05:58,480
So I'll make that guy bigger.

151
00:05:59,550 --> 00:06:03,060
So that's a pretty
interesting melange of images.

152
00:06:03,060 --> 00:06:05,460
A little bit compelling. (laughs)

153
00:06:05,460 --> 00:06:08,881
Makes the mind sort of
wonder and, you know, like,

154
00:06:08,881 --> 00:06:11,130
try to figure out what's going on.

155
00:06:11,130 --> 00:06:12,300
How are these all connected?

156
00:06:12,300 --> 00:06:15,060
But that's how you do what,
what did we call this?

157
00:06:15,060 --> 00:06:18,063
Insert pictures, illustrations,
equations, and symbols.

