﻿1
00:00:00,150 --> 00:00:02,130
- So now we're gonna take
a look at Custom Views.

2
00:00:02,130 --> 00:00:05,130
And it has been a long
day for me (laughs),

3
00:00:05,130 --> 00:00:07,590
but I still wanna keep going
and get this work done.

4
00:00:07,590 --> 00:00:08,580
So here we go.

5
00:00:08,580 --> 00:00:10,350
On the View ribbon, we have Custom Views.

6
00:00:10,350 --> 00:00:13,350
And Custom Views kind of makes me think of

7
00:00:13,350 --> 00:00:15,120
how we had the Scenario Manager.

8
00:00:15,120 --> 00:00:17,280
And the Scenario Manager allowed us,

9
00:00:17,280 --> 00:00:20,580
and remember we had the Scenario
Manager here under Data,

10
00:00:20,580 --> 00:00:22,890
What If Analysis, Scenario Manager,

11
00:00:22,890 --> 00:00:26,910
and the Scenario Manager allowed
us to add different inputs

12
00:00:26,910 --> 00:00:30,150
and have those change in
various scenarios, right?

13
00:00:30,150 --> 00:00:31,620
So we could have like,

14
00:00:31,620 --> 00:00:35,340
here is my scenario of the
order of the trips I wanna take.

15
00:00:35,340 --> 00:00:36,960
These are the places I wanna go.

16
00:00:36,960 --> 00:00:38,880
Here are the orders of the, you know,

17
00:00:38,880 --> 00:00:42,840
here's the order the ranking
of the locations my pop wants

18
00:00:42,840 --> 00:00:43,680
to go to, right?

19
00:00:43,680 --> 00:00:45,270
And I can save those different scenarios

20
00:00:45,270 --> 00:00:46,770
with the Scenario Manager.

21
00:00:46,770 --> 00:00:49,020
Here in Review, we have Custom Views.

22
00:00:49,020 --> 00:00:53,220
And Custom view allows you to
kind of define a custom view,

23
00:00:53,220 --> 00:00:54,750
not with inputs, right,

24
00:00:54,750 --> 00:00:56,610
but with all of this stuff here.

25
00:00:56,610 --> 00:00:59,640
So that includes hidden rows
and columns, cell selections,

26
00:00:59,640 --> 00:01:01,920
filter settings, window
settings, print settings,

27
00:01:01,920 --> 00:01:05,840
like page settings, margins,
headers and footers,

28
00:01:05,840 --> 00:01:06,810
and sheet settings.

29
00:01:06,810 --> 00:01:08,610
And so let's see this in action.

30
00:01:08,610 --> 00:01:10,470
So I could create one
custom view right here.

31
00:01:10,470 --> 00:01:12,090
This will be my first custom view,

32
00:01:12,090 --> 00:01:15,240
and I could call this
one or whatever I want.

33
00:01:15,240 --> 00:01:17,760
I'm gonna call it a AAA, triple A.

34
00:01:17,760 --> 00:01:20,760
And include in the view print
settings and hidden rows,

35
00:01:20,760 --> 00:01:21,960
columns, and filter settings.

36
00:01:21,960 --> 00:01:24,270
So there's my first
custom view right there.

37
00:01:24,270 --> 00:01:25,740
Now, if I go back to Custom Views,

38
00:01:25,740 --> 00:01:28,890
I've got that custom
view, AAA, right there,

39
00:01:28,890 --> 00:01:30,390
and I could add another one.

40
00:01:30,390 --> 00:01:32,400
To do that, I need to make some changes.

41
00:01:32,400 --> 00:01:33,750
So I'm gonna hide some columns.

42
00:01:33,750 --> 00:01:35,130
I'll hide those columns.

43
00:01:35,130 --> 00:01:36,600
I'm also gonna select some stuff.

44
00:01:36,600 --> 00:01:39,180
I'll select that, and I'll select that.

45
00:01:39,180 --> 00:01:41,220
And now I'm gonna go to Custom View,

46
00:01:41,220 --> 00:01:42,360
and I'm gonna go to Add,

47
00:01:42,360 --> 00:01:45,960
and I'm gonna call this BBB, and hit OK.

48
00:01:45,960 --> 00:01:47,430
And now when I go to Custom Views,

49
00:01:47,430 --> 00:01:48,280
I've got A and B,

50
00:01:48,280 --> 00:01:49,890
and I can choose to show one.

51
00:01:49,890 --> 00:01:51,690
So there's A, right?

52
00:01:51,690 --> 00:01:53,160
And then I can go back to Custom View,

53
00:01:53,160 --> 00:01:56,100
and I could choose to show
this one, and there's B.

54
00:01:56,100 --> 00:01:59,760
And so we could do all,
kind of see (laughs),

55
00:01:59,760 --> 00:02:00,960
see what they're doing, right?

56
00:02:00,960 --> 00:02:04,170
And we did the hidden rows and
columns and cell selections,

57
00:02:04,170 --> 00:02:07,380
but you could also do filter
settings, window settings,

58
00:02:07,380 --> 00:02:10,560
and print settings, like
page settings, margins,

59
00:02:10,560 --> 00:02:12,060
headers and footers, sheet settings.

60
00:02:12,060 --> 00:02:14,730
And the window settings
would be things like,

61
00:02:14,730 --> 00:02:17,880
maybe Formula Bar, Headings, Gridlines.

62
00:02:17,880 --> 00:02:21,240
And so let's just do some of
that to see that in action.

63
00:02:21,240 --> 00:02:23,730
And why, oh, you know what?

64
00:02:23,730 --> 00:02:25,800
I need my Headings (laughs).

65
00:02:25,800 --> 00:02:27,150
Like where did my headings go?

66
00:02:27,150 --> 00:02:28,560
So I'm gonna take all that stuff

67
00:02:28,560 --> 00:02:30,210
and I'm gonna hide three through 10.

68
00:02:30,210 --> 00:02:31,500
Hide three through 10.

69
00:02:31,500 --> 00:02:34,890
And I'll make this a custom view.

70
00:02:34,890 --> 00:02:37,260
And you know the formula bar is not there,

71
00:02:37,260 --> 00:02:38,100
right, on this one.

72
00:02:38,100 --> 00:02:42,243
So this custom view is
going to be add CCC, okay?

73
00:02:43,200 --> 00:02:44,730
And now when I go to Custom View,

74
00:02:44,730 --> 00:02:46,830
show A, there's A,

75
00:02:46,830 --> 00:02:48,360
and the formula bar is showing.

76
00:02:48,360 --> 00:02:50,670
And I would go to Custom View, show C.

77
00:02:50,670 --> 00:02:52,950
And there's C and the
formula bar is not showing.

78
00:02:52,950 --> 00:02:54,360
So that's what's being referred to,

79
00:02:54,360 --> 00:02:56,460
stuff like that with window settings.

80
00:02:56,460 --> 00:02:58,710
All right, so that's Custom Views,

81
00:02:58,710 --> 00:03:00,060
and they could be helpful

82
00:03:00,060 --> 00:03:02,790
if you wanna save one view of the data,

83
00:03:02,790 --> 00:03:05,250
you know, here's another view of the data.

84
00:03:05,250 --> 00:03:07,500
I wanna show step one in the presentation,

85
00:03:07,500 --> 00:03:09,570
and then show two in the presentation,

86
00:03:09,570 --> 00:03:11,640
and then show three in the presentation.

87
00:03:11,640 --> 00:03:14,010
So you could show different views

88
00:03:14,010 --> 00:03:15,150
or create different views

89
00:03:15,150 --> 00:03:16,920
for whatever purpose you needed to do.

90
00:03:16,920 --> 00:03:18,273
That's Custom Views.

