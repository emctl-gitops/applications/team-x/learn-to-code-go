package main

import (
	"fmt"

	"golang.org/x/exp/constraints"
)

type customers int

func main() {
	fmt.Println(sumInt(42, 43))
	fmt.Println(sumFloat64(42.03, 43.04))
	fmt.Println(sum[int](42, 43))
	fmt.Println(sum[float64](42.03, 43.04))

	var yesterday customers = 84
	var today customers = 168
	fmt.Println(sum(yesterday, today))
}

func sumInt(c, y int) int {
	return c + y
}

func sumFloat64(c, y float64) float64 {
	return c + y
}

type numType interface {
	constraints.Integer | constraints.Float
}

func sum[T numType](c, y T) T {
	return c + y
}
