package main

import (
	"fmt"

	"golang.org/x/exp/constraints"
)

type numType interface {
	constraints.Integer | constraints.Float
}

func sumNums[T numType](xt ...T) T {
	var total T
	for _, x := range xt {
		total += x
	}
	return total
}

func main() {
	fmt.Println(sumNums(11, 22, 33))
}
