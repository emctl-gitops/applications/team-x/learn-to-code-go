package main

import (
	"fmt"
	"os"
)

func main() {
	fmt.Println("all arguments\t", os.Args)
	fmt.Println("beyond main.go\t", os.Args[1:])

	for i, v := range os.Args[1:] {
		fmt.Printf("arguments %v is %v\n", i, v)
	}
}
