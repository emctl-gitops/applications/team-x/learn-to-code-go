package main

import (
	"bytes"
	"io"
	"os"
)

func main() {
	data := []byte("Hello, world!")
	r := bytes.NewReader(data)

	io.Copy(os.Stdout, r)
}
